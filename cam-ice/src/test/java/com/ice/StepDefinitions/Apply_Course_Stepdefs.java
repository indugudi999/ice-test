package com.ice.StepDefinitions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class Apply_Course_Stepdefs {
    public static WebDriver driver = new ChromeDriver();

    @Given("i want to apply for a specific ice course")
    public void iWantToApplyForASpecificIceCourse() {
        System.setProperty("webdriver.chrome.driver","C:\\work\\selenium\\drivers\\chromedriver");
        driver.get("https://www.ice.cam.ac.uk/course/undergraduate-certificate-immunology");
        driver.manage().window().maximize();
    }

    @Then("i click on apply now button")
    public void iClickOnApplyNowButton() throws InterruptedException {
        Thread.sleep(5000);
        driver.manage().timeouts().implicitlyWait(12, TimeUnit.SECONDS);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scroll(0,5000);");
        driver.findElement(By.xpath("//*[contains (@class, 'campl-primary-cta') and text() ='APPLY NOW']")).click();
        driver.manage().timeouts().implicitlyWait(1200, TimeUnit.SECONDS);
    }

    @When("i am on login page")
    public void iAmOnLoginPage() {
        String Actual = driver.getTitle();
        String Expected = "Login";

        if (Actual.equals(Expected)) {
            System.out.println("Test Passed!");
        } else {
            System.out.println("Test Failed");
        }
    }

    @And("i enter login details")
    public void iEnterLoginDetails() {
        driver.manage().timeouts().implicitlyWait(12,TimeUnit.SECONDS);
        driver.findElement(By.id("iceForm:emailAddressWrapper:emailAddress")).sendKeys("indugudi333@gmail.com");
        driver.manage().timeouts().implicitlyWait(12,TimeUnit.SECONDS);
        driver.findElement(By.id("iceForm:passwordWrapper:password")).sendKeys("Test@123");

    }

    @And("i click login button")
    public void iClickLoginButton() {
        driver.findElement(By.xpath("//*[contains (@class, 'ui-button-text ui-c') and text() ='Log in']")).click();
        driver.manage().timeouts().implicitlyWait(12,TimeUnit.SECONDS);
    }

    @And("I am on the Apply for a course section")
    public void iAmOnTheApplyForACourseSection() {
        String Actual = driver.getTitle();
        String Expected = "Course";

        if (Actual.equals(Expected)) {
            System.out.println("Test Passed!");
        } else {
            System.out.println("Test Failed");
        }

    }

    @And("I click the Start application button")
    public void iClickTheStartApplicationButton() {
        driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
        driver.findElement(By.id("iceForm:startApplication")).click();
    }

    @And("I am on the Contact Details section")
    public void iAmOnTheContactDetailsSection() {
        String Actual = driver.getTitle();
        String Expected = "Contact";

        if (Actual.equals(Expected)) {
            System.out.println("Test Passed!");
        } else {
            System.out.println("Test Failed");
        }

    }

    @And("I enter my phone number")
    public void iEnterMyPhoneNumber() {
        driver.manage().timeouts().implicitlyWait(12,TimeUnit.SECONDS);
        driver.findElement(By.id("iceForm:telephoneNumberWrapper:telephoneNumber")).sendKeys("123456789");
    }

    @And("I enter my Permanent residence address details")
    public void iEnterMyPermanentResidenceAddressDetails() {
        driver.manage().timeouts().implicitlyWait(12,TimeUnit.SECONDS);
        driver.findElement(By.xpath("//*[@id=\"iceForm:mainAddress:addrLine1Wrapper:addrLine1\"]")).sendKeys("enter the address ");
        driver.findElement(By.xpath("//*[@id=\"iceForm:mainAddress:townWrapper:town\"]")).sendKeys("enter the address ");
        driver.manage().timeouts().implicitlyWait(12,TimeUnit.SECONDS);
        driver.findElement(By.xpath("//*[@id=\"iceForm:mainAddress:postCodeWrapper:postCode\"]")).sendKeys("SE12RE");
        driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
        driver.findElement(By.xpath("//*[@id=\"iceForm:mainAddress:countryWrapper:country\"]/div[3]")).click();
        driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
        driver.findElement(By.xpath("//*[@id=\"iceForm:mainAddress:countryWrapper:country_1\"]/td")).click();
        driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);

     }

    @And("I select Yes Is your term time address the same as your permanent residence address?")


    @And("I click the Save and Next button")
    public void iClickTheSaveAndNextButton() {
        driver.findElement(By.id("iceForm:saveAndNext")).click();
    }

    @And("I am on the Education section")
    public void iAmOnTheEducationSection() {
        String Actual = driver.getTitle();
        String Expected = "Education";

        if (Actual.equals(Expected)) {
            System.out.println("Test Passed!");
        } else {
            System.out.println("Test Failed");
        }
    }

    @And("I enter my education details")
    public void iEnterMyEducationDetails() {
        driver.findElement(By.id("iceForm:education")).sendKeys("test");
    }

    @And("I am on the Personal Statement section")
    public void iAmOnThePersonalStatementSection() {
        String Actual = driver.getTitle();
        String Expected = "Personalstatment";

        if (Actual.equals(Expected)) {
            System.out.println("Test Passed!");
        } else {
            System.out.println("Test Failed");

        }
    }
}
