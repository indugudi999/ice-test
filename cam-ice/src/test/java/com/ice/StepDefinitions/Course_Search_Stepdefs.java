package com.ice.StepDefinitions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;


public class Course_Search_Stepdefs {


    public static WebDriver driver =new ChromeDriver();
    @Given("i am on ice course page")
    public static void iamonicecoursepage() {
        System.setProperty("webdriver.chrome.driver","C:\\work\\ice-test-automation\\src\\main\\resources\\chromedriver.exe");
        driver.get("https://www.ice.cam.ac.uk/courses/search");
        driver.manage().window().maximize();
}

    @When("i search for course")
    public static void iSearchForCourse() {
        WebDriverWait wait = new WebDriverWait(driver, 30);
        driver.findElement(By.xpath("//*[@id=\"edit-freetext\"]")).sendKeys("MSt in Entrepreneurship");
        driver.findElement(By.xpath("//*[@id=\"edit-submit\"]")).click();

            }

    @Then("i verify course in the result table")
    public void iVerifyCourseInTheResultTable() {
                 if (driver.getPageSource().contains("MSt in Entrepreneurship")) {
                     System.out.println("tex_is_tpresent");
                 }
                else {
            System.out.println("text_is_notpresent");
        }
}

    @And("i close the browser")
    public void iCloseTheBrowser() {
        driver.close();
    }
}


