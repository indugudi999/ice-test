Feature: Searching for ice course

  Scenario:  As an applicant i want to search for a course

    Given i am on ice course page
    When i search for course
    Then i verify course in the result table
    And i close the browser