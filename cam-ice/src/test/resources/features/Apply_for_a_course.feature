Feature: Applying for ice course

  Scenario:  As an applicant i want to apply for a course

    Given i want to apply for a specific ice course
    Then i click on apply now button
    When i am on login page
    And i enter login details
    And i click login button
    And I am on the Apply for a course section
    And I click the Start application button
    And I am on the Contact Details section
    And I enter my phone number
    And I enter my Permanent residence address details
    And I click the Save and Next button
    And I am on the Education section
    And I enter my education details
    And I click the Save and Next button
    And I am on the Personal Statement section
    Then i close the browserA